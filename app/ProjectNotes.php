<?php

namespace App;

use App\Observers\ProjectNotesObserver;
use Illuminate\Database\Eloquent\Model;
use App\Scopes\CompanyScope;

class ProjectNotes extends Model
{
    
    protected static function boot()
    {
        parent::boot();

        static::observe(ProjectNotesObserver::class);

        static::addGlobalScope(new CompanyScope);
    }

    

}