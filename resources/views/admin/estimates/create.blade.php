@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.estimates.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <style>
        .dropdown-content {
            width: 250px;
            max-height: 250px;
            overflow-y: scroll;
            overflow-x: hidden;
        }

        .customSR{
            display: flex;
            align-items: center;
            padding-top: 20px;
        }
        .customSR1{
            display: flex;
            align-items: center;
            justify-content: flex-end;
            width: 74%;
        }
        .custom-setting{
            width: 100%;
        }
        .custom-label{
            width: 125px;
            background: #F6F7F9;
            border-bottom: 1px solid #c0c0c0;
            margin-bottom: 0px;
            padding: 19px;
        }
        .custom-padding{
            border: 1px solid #efefef;
            padding: 12px;
            width: 70%;
        }


        .table .btn-danger, #sortable .btn-danger {
            background: #fff !important;
            border: 1px solid #c0c0c0 !important;
            border-radius: 20px !important;
            color: #c0c0c0 !important;
            width: 20px;
            height: 20px;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        #add-item{
            background: #00c292 !important;
            color: #fff !important;
            border: 0px !important;
            height: 35px;
            position: relative;
            top: -5px;
        }
        .custom-height{
            height: 30px !important;
        }
        .customWith2{
            display: flex;
            flex-direction: column;
            align-items: flex-end;
        }
        @media (max-width: 991px) {
            .customSR {
                display: flex;
                align-items: center;
                flex-direction: column;
                width: 100%;
            }
            .customSR1 {
                display: flex;
                align-items: center;
                justify-content: flex-end;
                flex-direction: column;
            }
            .custom-setting {
                width: 100%;
            }
            .custom-padding {
                border: 1px solid #efefef;
                padding: 12px;
                width: 100%;
            }
            .customWith{
                width: 100%;
            }
            .custom-label {
                width: 100%;
                background: #EFEFF6;
                border-bottom: 1px solid #c0c0c0;
                margin-bottom: 0px;
                padding: 19px;
            }
        }
        .panel .panel-body {
            padding-top: 0px !important;
            padding-left: 100px !important;
            padding-bottom: 0px !important;
            padding-right: 100px !important;
        }
    </style>
@endpush

@section('content')
    <div class="row">
        <div class="col-xs-12">

            <div class="panel panel-inverse">
                <div class="panel-heading"> @lang('modules.estimates.createEstimate')</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'storePayments','class'=>'ajax-form','method'=>'POST']) !!}
                        <div class="form-body">

                            <div class="customSR row">
                                <div class="col-md-6 customWith" style="display: flex;flex-direction: column;">
                                    <div class="customSR1 form-group" style="flex-direction: column;align-items: flex-start;">
                                        <label class="control-label">@lang('app.client')</label>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <select class="select2 form-control" data-placeholder="Choose Client" name="client_id">
                                                    @foreach($clients as $client)
                                                        <option
                                                                value="{{ $client->user_id }}">{{ ucwords(!is_null($client->company_name) ? $client->name . ' (' . $client->company_name .')' : $client->name) }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="customSR1 form-group" style="flex-direction: column;align-items: flex-start;">
                                        <label class="control-label">@lang('modules.invoices.currency')</label>
                                        <select class="form-control" name="currency_id" id="currency_id">
                                            @foreach($currencies as $currency)
                                                <option value="{{ $currency->id }}" @if($global->currency_id == $currency->id) selected @endif>{{ $currency->currency_symbol.' ('.$currency->currency_code.')' }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6 customWith customWith2">
                                    <div class="customSR1 form-group" style="margin-bottom: 0px;">
                                        <label class="custom-label control-label">@lang('app.estimate') #</label>
                                        <div class="custom-padding">
                                            <div class="custom-setting input-group">
                                                <div class="input-group-addon"><span class="invoicePrefix" data-prefix="{{ $invoiceSetting->estimate_prefix }}">{{ $invoiceSetting->estimate_prefix }}</span>#<span class="noOfZero" data-zero="{{ $invoiceSetting->estimate_digit }}">{{ $zero }}</span></div>
                                                <input type="text"  class="form-control readonly-background" readonly name="estimate_number" id="estimate_number" value="@if(is_null($lastEstimate))1 @else{{ ($lastEstimate) }}@endif">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="customSR1 form-group" style="margin-bottom: 0px;">
                                        <label class="custom-label control-label">@lang('modules.estimates.validTill')</label>
                                        <div class="custom-padding">
                                            <div class="custom-setting input-icon">
                                                <input type="text" class="form-control" name="valid_till" id="valid_till" value="{{ Carbon\Carbon::today()->addDays(30)->format($global->date_format) }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">

                                {{--                                <div class="col-xs-12  visible-md visible-lg">--}}

                                {{--                                        <div class="col-md-4 font-bold" style="padding: 8px 15px">--}}
                                {{--                                            @lang('modules.invoices.item')--}}
                                {{--                                        </div>--}}

                                {{--                                        <div class="col-md-1 font-bold" style="padding: 8px 15px">--}}
                                {{--                                            @lang('modules.invoices.qty')--}}
                                {{--                                        </div>--}}

                                {{--                                        <div class="col-md-2 font-bold" style="padding: 8px 15px">--}}
                                {{--                                            @lang('modules.invoices.unitPrice')--}}
                                {{--                                        </div>--}}

                                {{--                                        <div class="col-md-2 font-bold" style="padding: 8px 15px">--}}
                                {{--                                            @lang('modules.invoices.tax') <a href="javascript:;" id="tax-settings" ><i class="ti-settings text-info"></i></a>--}}
                                {{--                                        </div>--}}

                                {{--                                        <div class="col-md-2 text-center font-bold" style="padding: 8px 15px">--}}
                                {{--                                            @lang('modules.invoices.amount')--}}
                                {{--                                        </div>--}}

                                {{--                                        <div class="col-md-1" style="padding: 8px 15px">--}}
                                {{--                                            &nbsp;--}}
                                {{--                                        </div>--}}

                                {{--                                </div>--}}
                                <div class="table-responsive">
                                    <table class="table item-row">
                                        <thead>
                                        <tr style="background:#F6F7F9;">
                                            <th></th>
                                            <th style="font-weight: normal;color: #000;">@lang('modules.invoices.item')</th>
                                            <th style="font-weight: normal;color: #000;">@lang('app.description')</th>
                                            <th style="font-weight: normal;color: #000;">@lang('modules.invoices.qty')</th>
                                            <th style="font-weight: normal;color: #000;">@lang('modules.invoices.unitPrice')</th>
                                            <th style="font-weight: normal;color: #000;">@lang('modules.invoices.tax')
                                                <a href="javascript:;" id="tax-settings"><i class="ti-settings text-info"></i></a>
                                            </th>
                                            <th style="font-weight: normal;color: #000;">@lang('modules.invoices.amount')</th>
                                        </tr>
                                        </thead>
                                        <tbody id="sortable">
                                        <tr>
                                            <td>
                                                <div class="col-md-1 text-right visible-md visible-lg" style="height:40px;width: 50px;display: flex !important;align-items: center;">
                                                    <button type="button" class="btn remove-item btn-circle btn-danger"><i class="fa fa-remove"></i></button>
                                                </div>
                                                <div class="col-md-1 hidden-md hidden-lg" style="display: none;">
                                                    <div class="row">
                                                        <button type="button" class="btn btn-circle remove-item btn-danger"><i class="fa fa-remove"></i> @lang('app.remove')</button>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <input type="text" class="custom-height form-control item_name" name="item_name[]">
                                            </td>
                                            <td>
                                                <textarea name="item_summary[]" class="custom-height form-control" placeholder="@lang('app.description')" rows="2"></textarea>
                                            </td>
                                            <td>
                                                <input type="number" min="1" class="custom-height form-control quantity" value="1" name="quantity[]" >
                                            </td>
                                            <td>
                                                <input type="text"  class="custom-height form-control cost_per_item" name="cost_per_item[]" value="0" >
                                            </td>
                                            <td>
                                                <select id="multiselect" name="taxes[0][]"  multiple="multiple" class="custom-height selectpicker form-control type">
                                                    @foreach($taxes as $tax)
                                                        <option data-rate="{{ $tax->rate_percent }}" value="{{ $tax->id }}">{{ $tax->tax_name }}: {{ $tax->rate_percent }}%</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>
                                                <p class="form-control-static" style="    text-align: end;
    padding-right: 20px;
    font-weight: 500;min-height: 40px;background:transparent;">
                                                    <span class="amount-html">0.00</span>
                                                </p>
                                                <input type="hidden" class="amount" name="amount[]" value="0">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div id="sortable">
                                    {{--                                <div class="col-xs-12 item-row margin-top-5">--}}

                                    {{--                                    <div class="col-md-4">--}}
                                    {{--                                        <div class="row">--}}
                                    {{--                                            <div class="form-group">--}}
                                    {{--                                                <label class="control-label hidden-md hidden-lg">@lang('modules.invoices.item')</label>--}}
                                    {{--                                                <div class="input-group">--}}
                                    {{--                                                    <div class="input-group-addon"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span></div>--}}
                                    {{--                                                    <input type="text" class="form-control item_name" name="item_name[]">--}}
                                    {{--                                                </div>--}}
                                    {{--                                            </div>--}}
                                    {{--                                            <div class="form-group">--}}
                                    {{--                                                    <textarea name="item_summary[]" class="form-control" placeholder="@lang('app.description')" rows="2"></textarea>--}}

                                    {{--                                            </div>--}}
                                    {{--                                        </div>--}}

                                    {{--                                    </div>--}}



                                    {{--                                    <div class="col-md-1">--}}

                                    {{--                                        <div class="form-group">--}}
                                    {{--                                            <label class="control-label hidden-md hidden-lg">@lang('modules.invoices.qty')</label>--}}
                                    {{--                                            <input type="text" min="0" class="form-control quantity" value="1" name="quantity[]" >--}}
                                    {{--                                        </div>--}}


                                    {{--                                    </div>--}}

                                    {{--                                    <div class="col-md-2">--}}
                                    {{--                                        <div class="row">--}}
                                    {{--                                            <div class="form-group">--}}
                                    {{--                                                <label class="control-label hidden-md hidden-lg">@lang('modules.invoices.unitPrice')</label>--}}
                                    {{--                                                <input type="text" min="" class="form-control cost_per_item" name="cost_per_item[]" value="0" >--}}
                                    {{--                                            </div>--}}
                                    {{--                                        </div>--}}

                                    {{--                                    </div>--}}

                                    {{--                                    <div class="col-md-2">--}}
                                    {{--                                        <div class="form-group">--}}
                                    {{--                                            <label class="control-label hidden-md hidden-lg">@lang('modules.invoices.type')</label>--}}
                                    {{--                                            <select id="multiselect" name="taxes[0][]"  multiple="multiple" class="selectpicker form-control type">--}}
                                    {{--                                                @foreach($taxes as $tax)--}}
                                    {{--                                                    <option data-rate="{{ $tax->rate_percent }}" value="{{ $tax->id }}">{{ $tax->tax_name }}: {{ $tax->rate_percent }}%</option>--}}
                                    {{--                                                @endforeach--}}
                                    {{--                                            </select>--}}
                                    {{--                                        </div>--}}
                                    {{--                                    </div>--}}

                                    {{--                                    <div class="col-md-2 border-dark  text-center">--}}
                                    {{--                                        <label class="control-label hidden-md hidden-lg">@lang('modules.invoices.amount')</label>--}}

                                    {{--                                        <p class="form-control-static"><span class="amount-html">0.00</span></p>--}}
                                    {{--                                        <input type="hidden" class="amount" name="amount[]">--}}
                                    {{--                                    </div>--}}

                                    {{--                                    <div class="col-md-1 text-right visible-md visible-lg">--}}
                                    {{--                                        <button type="button" class="btn remove-item btn-circle btn-danger"><i class="fa fa-remove"></i></button>--}}
                                    {{--                                    </div>--}}
                                    {{--                                    <div class="col-md-1 hidden-md hidden-lg">--}}
                                    {{--                                        <div class="row">--}}
                                    {{--                                            <button type="button" class="btn remove-item btn-danger"><i class="fa fa-remove"></i> @lang('app.remove')</button>--}}
                                    {{--                                        </div>--}}
                                    {{--                                    </div>--}}

                                    {{--                                </div>--}}
                                </div>

                                <div id="item-list">

                                </div>

                                <div class="col-xs-12 m-t-5">
                                    <button type="button" class="btn btn-info" id="add-item"><i class="fa fa-plus"></i> @lang('modules.invoices.addItem')</button>
                                    <div class="btn-group m-b-10">
                                        <button aria-expanded="false" data-toggle="dropdown" class="btn btn-info dropdown-toggle waves-effect waves-light" type="button">@lang('app.menu.products') <span class="caret"></span></button>
                                        <ul role="menu" class="dropdown-menu dropdown-content">
                                            @foreach($products as $product)
                                                <li class="m-b-10">
                                                    <div class="row m-t-10">
                                                        <div class="col-md-6" style="padding-left: 30px">
                                                            {{ $product->name }}
                                                        </div>
                                                        <div class="col-md-6" style="text-align: right;padding-right: 30px;">
                                                            <a href="javascript:;" data-pk="{{ $product->id }}" class="btn btn-success btn btn-outline btn-xs waves-effect add-product">@lang('app.add') <i class="fa fa-plus" aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>

                                {{--                                <div class="col-xs-12 ">--}}
                                {{--                                    <div class="row">--}}
                                {{--                                        <div class="col-md-offset-9 col-xs-6 col-md-1 text-right p-t-10" >@lang('modules.invoices.subTotal')</div>--}}

                                {{--                                        <p class="form-control-static col-xs-6 col-md-2" >--}}
                                {{--                                            <span class="sub-total">0.00</span>--}}
                                {{--                                        </p>--}}


                                {{--                                        <input type="hidden" class="sub-total-field" name="sub_total" value="0">--}}
                                {{--                                    </div>--}}

                                {{--                                    <div class="row">--}}
                                {{--                                        <div class="col-md-offset-9 col-md-1 text-right p-t-10">--}}
                                {{--                                            @lang('modules.invoices.discount')--}}
                                {{--                                        </div>--}}
                                {{--                                        <div class="form-group col-xs-6 col-md-1" >--}}
                                {{--                                            <input type="number" min="0" value="0" name="discount_value" class="form-control discount_value">--}}
                                {{--                                        </div>--}}
                                {{--                                        <div class="form-group col-xs-6 col-md-1" >--}}
                                {{--                                            <select class="form-control" name="discount_type" id="discount_type">--}}
                                {{--                                                <option value="percent">%</option>--}}
                                {{--                                                <option value="fixed">@lang('modules.invoices.amount')</option>--}}
                                {{--                                            </select>--}}
                                {{--                                        </div>--}}
                                {{--                                    </div>--}}

                                {{--                                    <div class="row m-t-5" id="invoice-taxes">--}}
                                {{--                                        <div class="col-md-offset-9 col-md-1 text-right p-t-10">--}}
                                {{--                                            @lang('modules.invoices.tax')--}}
                                {{--                                        </div>--}}

                                {{--                                        <p class="form-control-static col-xs-6 col-md-2" >--}}
                                {{--                                            <span class="tax-percent">0.00</span>--}}
                                {{--                                        </p>--}}
                                {{--                                    </div>--}}

                                {{--                                    <div class="row m-t-5 font-bold">--}}
                                {{--                                        <div class="col-md-offset-9 col-md-1 col-xs-6 text-right p-t-10" >@lang('modules.invoices.total')</div>--}}

                                {{--                                        <p class="form-control-static col-xs-6 col-md-2" >--}}
                                {{--                                            <span class="total">0.00</span>--}}
                                {{--                                        </p>--}}


                                {{--                                        <input type="hidden" class="total-field" name="total" value="0">--}}
                                {{--                                    </div>--}}

                                {{--                                </div>--}}
                                <div class="col-xs-12 ">
                                    <div class="row" style="
                                        border-top: 1px solid #fff;
                                        border-left: 1px solid #fff;
                                        border-right: 1px solid #fff;
                                        border-bottom: 1px solid #fff;
                                    ">
                                        <div class="col-md-7">

                                        </div>
                                        <div class="col-md-5" style="background:#F6F7F9;">
                                            <div style="padding: 9px;
                                                margin-top:0px;
                                                text-align: end;
                                                margin-left: 8px;
                                                margin-right: 8px;">
                                                <span style="font-size: 13px;padding-right:15px;">@lang('modules.invoices.subTotal') $ &nbsp;&nbsp; :</span>
                                                <span class="sub-total" style="font-weight: bold;">0.00</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="
                                        border-left: 1px solid #fff;
                                        border-right: 1px solid #fff;
                                        border-bottom: 1px solid #fff;
                                    ">
                                        <div class="col-md-7">

                                        </div>
                                        <div class="col-md-5" style="background:#F6F7F9;">
                                            <div style="padding: 9px;
                                            text-align: end;
                                            margin-left: 8px;
                                            margin-right: 8px;">
                                                <span style="font-size: 13px;padding-right:15px;">
                                                    @if(\App::getLocale() == 'nl')
                                                        Korting
                                                    @else
                                                        discount
                                                    @endif
                                                    &nbsp;&nbsp;
                                                    <input type="number" style="width:70px;
        display: inline-block;" min="0" value="0" name="discount_value" class="form-control discount_value">
                                                    <select class="form-control" style="width:107px;
            display: inline-block;" name="discount_type" id="discount_type">
                                                        <option value="percent">%</option>
                                                        <option value="fixed">@lang('modules.invoices.amount')</option>
                                                    </select>

                                                    :</span>
                                                {{--                                            <span class="tax-percent">0.00</span>--}}
                                                <span class="total">0.00</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="
                                        border-bottom: 1px solid #fff;
                                        border-left: 1px solid #fff;
                                        border-right: 1px solid #fff;
                                    ">
                                        <div class="col-md-7">

                                        </div>
                                        <div class="col-md-5" style="background:#F6F7F9;" id="invoice-taxes">
                                            <div style="padding: 9px;
                                                text-align: end;
                                                margin-left: 8px;
                                                margin-right: 8px;">
                                                <div style="font-size: 13px;padding-right:15px;" >
                                                    <span style="position:relative;right:10px;">@lang('modules.invoices.tax')</span> : <span style="position:relative;left:17px;">0.00</span></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="
                                        border-bottom: 1px solid #fff;
                                        border-left: 1px solid #fff;
                                        border-right: 1px solid #fff;
                                    ">
                                        <div class="col-md-7">

                                        </div>
                                        <div class="col-md-5" style="background:#F6F7F9;">
                                            <div style="padding: 9px;
                                                text-align: end;
                                                margin-left: 8px;
                                                margin-right: 8px;">
                                                <span style="font-weight:normal;font-size: 13px;padding-right:15px;">@lang('modules.invoices.total') $ &nbsp;&nbsp; :</span>
                                                <span class="total" style="font-weight: normal;">0.00</span>
                                            </div>
                                            <input type="hidden" class="sub-total-field" name="sub_total" value="0">
                                            <input type="hidden" class="total-field" name="total" value="0">
                                        </div>
                                    </div>

                                    {{--                                    <div class="row">--}}
                                    {{--                                        <div class="col-md-6">--}}

                                    {{--                                        </div>--}}
                                    {{--                                        <div class="col-md-6">--}}
                                    {{--                                            <table class="table">--}}
                                    {{--                                                <tr>--}}
                                    {{--                                                    <td style="border-left: 1px solid #efefef !important;width: 20%;"></td>--}}
                                    {{--                                                    <td style="text-align: end;">Sub Total</td>--}}
                                    {{--                                                    <td style="text-align: end;border: 1px solid #efefef !important;">--}}
                                    {{--                                                        <span class="sub-total">0.00</span>--}}
                                    {{--                                                    </td>--}}
                                    {{--                                                </tr>--}}
                                    {{--                                                <tr>--}}
                                    {{--                                                    <td style="    padding-top: 13px !important;border-left: 1px solid #efefef !important;width:20%;text-align: end;">--}}
                                    {{--                                                        @lang('modules.invoices.discount')--}}
                                    {{--                                                    </td>--}}
                                    {{--                                                    <td style="width: 20%;    border: 1px solid #efefef !important;text-align: center">--}}
                                    {{--                                                        <input type="number" style="width:70px;--}}
                                    {{--    display: inline-block;" min="0" value="0" name="discount_value" class="form-control discount_value">--}}
                                    {{--                                                        <select class="form-control" style="width:70px;--}}
                                    {{--    display: inline-block;" name="discount_type" id="discount_type">--}}
                                    {{--                                                            <option value="percent">%</option>--}}
                                    {{--                                                            <option value="fixed">@lang('modules.invoices.amount')</option>--}}
                                    {{--                                                        </select>--}}
                                    {{--                                                    </td>--}}
                                    {{--                                                    <td style="    padding-top: 13px !important;width:20%;text-align: end;border: 1px solid #efefef !important;">$0.00</td>--}}
                                    {{--                                                </tr>--}}
                                    {{--                                                <tr style="background: #c0c0c0; font-weight: bold !important;">--}}
                                    {{--                                                    <td style="border-left: 1px solid #efefef !important;width:20%;text-align: end;">--}}
                                    {{--                                                    </td>--}}
                                    {{--                                                    <td style="width:20%;text-align: end;">--}}
                                    {{--                                                        @lang('modules.invoices.total')--}}
                                    {{--                                                    </td>--}}
                                    {{--                                                    <td style="border: 1px solid #efefef !important;width:20%;text-align: end;">--}}
                                    {{--                                                        <span class="total">0.00</span>--}}
                                    {{--                                                    </td>--}}
                                    {{--                                                </tr>--}}
                                    {{--                                            </table>--}}
                                    {{--                                        </div>--}}
                                    {{--                                        <div class="col-md-offset-9 col-xs-6 col-md-1 text-right p-t-10" >@lang('modules.invoices.subTotal')</div>--}}

                                    {{--                                        <p class="form-control-static col-xs-6 col-md-2" >--}}
                                    {{--                                            <span class="sub-total">0.00</span>--}}
                                    {{--                                        </p>--}}


                                    {{--                                    </div>--}}

                                    {{--                                    <div class="row">--}}
                                    {{--                                        <div class="col-md-offset-9 col-md-1 text-right p-t-10">--}}
                                    {{--                                            @lang('modules.invoices.discount')--}}
                                    {{--                                        </div>--}}
                                    {{--                                        <div class="form-group col-xs-6 col-md-1" >--}}
                                    {{--                                            <input type="number" min="0" value="0" name="discount_value" class="form-control discount_value">--}}
                                    {{--                                        </div>--}}
                                    {{--                                        <div class="form-group col-xs-6 col-md-1" >--}}
                                    {{--                                            <select class="form-control" name="discount_type" id="discount_type">--}}
                                    {{--                                                <option value="percent">%</option>--}}
                                    {{--                                                <option value="fixed">@lang('modules.invoices.amount')</option>--}}
                                    {{--                                            </select>--}}
                                    {{--                                        </div>--}}
                                    {{--                                    </div>--}}

                                    {{--                                    <div class="row m-t-5" id="invoice-taxes">--}}
                                    {{--                                        <div class="col-md-offset-9 col-md-1 text-right p-t-10">--}}
                                    {{--                                            @lang('modules.invoices.tax')--}}
                                    {{--                                        </div>--}}

                                    {{--                                        <p class="form-control-static col-xs-6 col-md-2" >--}}
                                    {{--                                            <span class="tax-percent">0.00</span>--}}
                                    {{--                                        </p>--}}
                                    {{--                                    </div>--}}

                                    <div class="row m-t-5 font-bold">
                                        {{--                                        <div class="col-md-offset-9 col-md-1 col-xs-6 text-right p-t-10" >@lang('modules.invoices.total')</div>--}}

                                        {{--                                        <p class="form-control-static col-xs-6 col-md-2" >--}}
                                        {{--                                            <span class="total">0.00</span>--}}
                                        {{--                                        </p>--}}


                                        {{--                                        <input type="hidden" class="total-field" name="total" value="0">--}}
                                    </div>

                                </div>
                            </div>


                            <div class="row">

                                <div class="col-sm-12" style="padding: 0px;">

                                    <div class="form-group">
                                        <label class="control-label">@lang('app.note')</label>

                                        <textarea name="note" class="form-control" rows="5"></textarea>
                                    </div>

                                </div>

                            </div>

                        </div>
                        <div class="form-actions" style="margin-top: 0px">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="dropup">
                                        <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            @lang('app.save') <span class="caret"></span>
                                        </button>
                                        <ul role="menu" class="dropdown-menu">
                                            <li>
                                                <a href="javascript:;" class="save-form" data-type="save">
                                                    <i class="fa fa-save"></i> @lang('app.save')
                                                </a>
                                            </li>
                                            <li class="divider"></li>
                                            <li>
                                                <a href="javascript:;" class="save-form" data-type="draft">
                                                    <i class="fa fa-file"></i> @lang('app.saveDraft')
                                                </a>
                                            </li>
                                            <li class="divider"></li>
                                            <li>
                                                <a href="javascript:void(0);" class="save-form" data-type="send">
                                                    <i class="fa fa-send"></i> @lang('app.saveSend')
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="taxModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    @lang('app.loading')
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">@lang('app.close')</button>
                    <button type="button" class="btn blue">@lang('app.save') @lang('app.changes')</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal Ends--}}
@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>

    <script>
        $(function () {
            $( "#sortable" ).sortable();
        });

        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });

        jQuery('#valid_till').datepicker({
            autoclose: true,
            todayHighlight: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });

        $('.save-form').click(function(){
            var type = $(this).data('type');
            calculateTotal();

            $.easyAjax({
                url:'{{route('admin.estimates.store')}}',
                container:'#storePayments',
                type: "POST",
                redirect: true,
                data:$('#storePayments').serialize()+ "&type=" + type
            })
        });

        $('#add-item').click(function () {
            var i = $(document).find('.item_name').length;
            var item = '<tr class="item-row">' +
                '<td>' +
                '    <div class="col-md-1 text-right visible-md visible-lg" style="height:40px;width: 50px;display: flex !important;align-items: center;">' +
                '        <button type="button" class="btn remove-item btn-circle btn-danger"><i class="fa fa-remove"></i></button>' +
                '    </div>' +
                '    <div class="col-md-1 hidden-md hidden-lg" style="display: none;">' +
                '        <div class="row">' +
                '            <button type="button" class="btn btn-circle remove-item btn-danger"><i class="fa fa-remove"></i> @lang('app.remove')</button>' +
                '        </div>' +
                '    </div>' +
                '</td>' +
                '<td>' +
                '    <input type="text" class="custom-height form-control item_name" name="item_name[]">' +
                '</td>' +
                '<td>' +
                '    <textarea name="item_summary[]" class="custom-height form-control" placeholder="@lang('app.description')" rows="2"></textarea>' +
                '</td>' +
                '<td>' +
                '    <input type="number" min="1" class="custom-height form-control quantity" value="1" name="quantity[]" >' +
                '</td>' +
                '<td>' +
                '    <input type="text"  class="custom-height form-control cost_per_item" name="cost_per_item[]" value="0" >' +
                '</td>' +
                '<td>' +
                '    <select id="multiselect'+i+'" name="taxes['+i+'][]"  multiple="multiple" class="custom-height selectpicker form-control type">' +
                '        @foreach($taxes as $tax)' +
                '            <option data-rate="{{ $tax->rate_percent }}" value="{{ $tax->id }}">{{ $tax->tax_name }}: {{ $tax->rate_percent }}%</option>' +
                '        @endforeach' +
                '    </select>' +
                '</td>' +
                '<td>' +
                '    <p class="form-control-static" style="    text-align: end;' +
                '    padding-right: 20px;' +
                '    font-weight: 500;min-height: 40px;background:transparent;">' +
                '        <span class="amount-html">0.00</span>' +
                '    </p>' +
                '    <input type="hidden" class="amount" name="amount[]" value="0">' +
                '</td>' +
                '</tr>';

            {{--'<div class="col-xs-12 item-row margin-top-5">'--}}

            {{--+'<div class="col-md-4">'--}}
            {{--+'<div class="row">'--}}
            {{--+'<div class="form-group">'--}}
            {{--+'<label class="control-label hidden-md hidden-lg">@lang('modules.invoices.item')</label>'--}}
            {{--+'<div class="input-group">'--}}
            {{--+'<div class="input-group-addon"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span></div>'--}}
            {{--+'<input type="text" class="form-control item_name" name="item_name[]" >'--}}
            {{--+'</div>'--}}
            {{--+'</div>'--}}

            {{--+'<div class="form-group">'--}}
            {{--+'<textarea name="item_summary[]" class="form-control" placeholder="@lang('app.description')" rows="2"></textarea>'--}}
            {{--+'</div>'--}}

            {{--+'</div>'--}}

            {{--+'</div>'--}}

            {{--+'<div class="col-md-1">'--}}

            {{--+'<div class="form-group">'--}}
            {{--+'<label class="control-label hidden-md hidden-lg">@lang('modules.invoices.qty')</label>'--}}
            {{--+'<input type="number" min="1" class="form-control quantity" value="1" name="quantity[]" >'--}}
            {{--+'</div>'--}}


            {{--+'</div>'--}}

            {{--+'<div class="col-md-2">'--}}
            {{--+'<div class="row">'--}}
            {{--+'<div class="form-group">'--}}
            {{--+'<label class="control-label hidden-md hidden-lg">@lang('modules.invoices.unitPrice')</label>'--}}
            {{--+'<input type="text" min="0" class="form-control cost_per_item" value="0" name="cost_per_item[]">'--}}
            {{--+'</div>'--}}
            {{--+'</div>'--}}

            {{--+'</div>'--}}


            {{--+'<div class="col-md-2">'--}}

            {{--+'<div class="form-group">'--}}
            {{--+'<label class="control-label hidden-md hidden-lg">@lang('modules.invoices.tax')</label>'--}}
            {{--+'<select id="multiselect'+i+'" name="taxes['+i+'][]" value="null"  multiple="multiple" class="selectpicker form-control type">'--}}
            {{--    @foreach($taxes as $tax)--}}
            {{--+'<option data-rate="{{ $tax->rate_percent }}" value="{{ $tax->id }}">{{ $tax->tax_name.': '.$tax->rate_percent }}%</option>'--}}
            {{--    @endforeach--}}
            {{--+'</select>'--}}
            {{--+'</div>'--}}


            {{--+'</div>'--}}

            {{--+'<div class="col-md-2 text-center">'--}}
            {{--+'<label class="control-label hidden-md hidden-lg">@lang('modules.invoices.amount')</label>'--}}
            {{--+'<p class="form-control-static"><span class="amount-html">0.00</span></p>'--}}
            {{--+'<input type="hidden" class="amount" name="amount[]">'--}}
            {{--+'</div>'--}}

            {{--+'<div class="col-md-1 text-right visible-md visible-lg">'--}}
            {{--+'<button type="button" class="btn remove-item btn-circle btn-danger"><i class="fa fa-remove"></i></button>'--}}
            {{--+'</div>'--}}

            {{--+'<div class="col-md-1 hidden-md hidden-lg">'--}}
            {{--+'<div class="row">'--}}
            {{--+'<button type="button" class="btn remove-item btn-danger"><i class="fa fa-remove"></i> @lang('app.remove')</button>'--}}
            {{--+'</div>'--}}
            {{--+'</div>'--}}

            {{--+'</div>';--}}

            $(item).hide().appendTo("#sortable").fadeIn(500);
            $('#multiselect'+i).selectpicker();
        });

        $('#storePayments').on('click','.remove-item', function () {
            $(this).closest('.item-row').fadeOut(300, function() {
                $(this).remove();
                $('.item-row').each(function(index){
                    $(this).find('.selectpicker').attr('name', 'taxes['+index+'][]');
                    $(this).find('.selectpicker').attr('id', 'multiselect'+index);
                });
                calculateTotal();
            });
        });

        $('#storePayments').on('keyup change','.quantity,.cost_per_item,.item_name, .discount_value', function () {
            var quantity = $(this).closest('.item-row').find('.quantity').val();

            var perItemCost = $(this).closest('.item-row').find('.cost_per_item').val();

            var amount = (quantity*perItemCost);

            $(this).closest('.item-row').find('.amount').val(decimalupto2(amount).toFixed(2));
            $(this).closest('.item-row').find('.amount-html').html(decimalupto2(amount).toFixed(2));

            calculateTotal();


        });

        $('#storePayments').on('change','.type, #discount_type', function () {
            var quantity = $(this).closest('.item-row').find('.quantity').val();

            var perItemCost = $(this).closest('.item-row').find('.cost_per_item').val();

            var amount = (quantity*perItemCost);

            $(this).closest('.item-row').find('.amount').val(decimalupto2(amount).toFixed(2));
            $(this).closest('.item-row').find('.amount-html').html(decimalupto2(amount).toFixed(2));

            calculateTotal();


        });

        function calculateTotal()
        {
            var subtotal = 0;
            var discount = 0;
            var tax = '';
            var taxList = new Object();
            var taxTotal = 0;
            var discountType = $('#discount_type').val();
            var discountValue = $('.discount_value').val();

            $(".quantity").each(function (index, element) {
                var itemTax = [];
                var itemTaxName = [];
                var discountedAmount = 0;

                $(this).closest('.item-row').find('select.type option:selected').each(function (index) {
                    itemTax[index] = $(this).data('rate');
                    itemTaxName[index] = $(this).text();
                });
                var itemTaxId = $(this).closest('.item-row').find('select.type').val();

                var amount = parseFloat($(this).closest('.item-row').find('.amount').val());
                if(discountType == 'percent' && discountValue != ''){
                    discountedAmount = parseFloat(amount - ((parseFloat(amount)/100)*parseFloat(discountValue)));
                }

                if(isNaN(amount)){ amount = 0; }

                subtotal = (parseFloat(subtotal)+parseFloat(amount)).toFixed(2);

                if(itemTaxId != ''){
                    for(var i = 0; i<=itemTaxName.length; i++)
                    {
                        if(typeof (taxList[itemTaxName[i]]) === 'undefined'){
                            if (discountedAmount > 0) {
                                taxList[itemTaxName[i]] = ((parseFloat(itemTax[i])/100)*parseFloat(discountedAmount));
                            } else {
                                taxList[itemTaxName[i]] = ((parseFloat(itemTax[i])/100)*parseFloat(amount));
                            }
                        }
                        else{
                            if (discountedAmount > 0) {
                                taxList[itemTaxName[i]] = parseFloat(taxList[itemTaxName[i]]) + ((parseFloat(itemTax[i])/100)*parseFloat(discountedAmount));
                                console.log(taxList[itemTaxName[i]]);

                            } else {
                                taxList[itemTaxName[i]] = parseFloat(taxList[itemTaxName[i]]) + ((parseFloat(itemTax[i])/100)*parseFloat(amount));
                            }
                        }
                    }
                }
            });


            $.each( taxList, function( key, value ) {
                if(!isNaN(value)){
                    tax = tax+'<div class="col-md-offset-8 col-md-2 text-right p-t-10" style="font-weight: normal;">'
                        +key
                        +'</div>'
                        +'<p class="form-control-static col-xs-6 col-md-2" style="text-align: center;">'
                        +'<span class="tax-percent" style="font-weight: normal;">'+(decimalupto2(value)).toFixed(2)+'</span>'
                        +'</p>';
                    taxTotal = taxTotal+decimalupto2(value);
                }
            });

            if(isNaN(subtotal)){  subtotal = 0; }

            $('.sub-total').html(decimalupto2(subtotal).toFixed(2));
            $('.sub-total-field').val(decimalupto2(subtotal));



            if(discountValue != ''){
                if(discountType == 'percent'){
                    discount = ((parseFloat(subtotal)/100)*parseFloat(discountValue));
                }
                else{
                    discount = parseFloat(discountValue);
                }

            }

//       show tax
            $('#invoice-taxes').html(tax);

//            calculate total
            var totalAfterDiscount = decimalupto2(subtotal-discount);
            // console.log(totalAfterDiscount, taxTotal);

            totalAfterDiscount = (totalAfterDiscount < 0) ? 0 : totalAfterDiscount;

            var total = decimalupto2(totalAfterDiscount+taxTotal);

            $('.total').html(total.toFixed(2));
            $('.total-field').val(total.toFixed(2));

        }

        $('#tax-settings').click(function () {
            var url = '{{ route('admin.taxes.create')}}';
            $('#modelHeading').html('Manage Project Category');
            $.ajaxModal('#taxModal', url);
        })

        function decimalupto2(num) {
            var amt =  Math.round(num * 100) / 100;
            return parseFloat(amt.toFixed(2));
        }

        $('.add-product').on('click', function(event) {
            event.preventDefault();
            var id = $(this).data('pk');
            var currencyId = $('#currency_id').val();
            $.easyAjax({
                url:'{{ route('admin.all-invoices.update-item') }}',
                type: "GET",
                data: { id: id, currencyId: currencyId },
                success: function(response) {
                    $(response.view).hide().appendTo("#sortable").fadeIn(500);
                    var noOfRows = $(document).find('#sortable .item-row').length;
                    var i = $(document).find('.item_name').length-1;
                    var itemRow = $(document).find('#sortable .item-row:nth-child('+noOfRows+') select.type');
                    itemRow.attr('id', 'multiselect'+i);
                    itemRow.attr('name', 'taxes['+i+'][]');
                    $(document).find('#multiselect'+i).selectpicker();
                    calculateTotal();
                    calculateTotal();
                }
            });
        });

    </script>
@endpush
