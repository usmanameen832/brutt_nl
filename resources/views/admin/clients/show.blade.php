@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.clients.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.menu.projects')</li>
            </ol>
        </div>
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12 text-right">

            <a href="{{ route('admin.clients.edit',$clientDetail->id) }}"
               class="btn btn-outline btn-success btn-sm">@lang('modules.lead.edit')
                <i class="fa fa-edit" aria-hidden="true"></i></a>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

<style>
    .details{
        border: 1px solid silver;
        margin-bottom: 40px;
    }

    .details h5{
        font-weight: bold;
        color: grey;
    }

    .details h4{
        font-weight: bold;
        font-size: 14px;
    }

    .details p{
        font-weight: 400;
        font-size: 14px;
    }

    .details hr{
        color: #74777B !important;
    }

    .show h6{
        font-weight: bold;
        font-size: 12px;
        margin-left: 10px;
        margin-top: 15px;
    }

    .project strong{
        color: grey;
    }

    .project .details-btn{
        background-color: #DBF3FA;
        color: #4DA9F3;
        border-radius: 20px;
        font-weight: 600;
        border: none;
        font-size: 12px;
    }

    .tabs-style-line nav a {
        padding: 20px 10px;
         box-shadow: none !important;
        color: #686868;
        text-align: left;
        text-transform: uppercase;
        letter-spacing: 1px;
        line-height: 1;
        -webkit-transition: color .3s,box-shadow .3s;
        transition: color .3s,box-shadow .3s;
    }

    .project table {
        border-collapse: collapse;
        width: 100%;
    }

    .project th{
        font-size: 12px;
        padding: 8px;
        padding-left: 18px;
        text-align: left;
        border-bottom: 2px solid #ddd;
    }

    .project td {
        padding: 12px 0px 12px 18px;
        font-size: 12px;
        border-bottom: 1px solid #ddd;
    }

    .contacts table {
        border-collapse: collapse;
        width: 100%;
    }

    .contacts th{
        font-size: 12px;
        padding: 8px;
        padding-left: 18px;
        text-align: left;

    }

    .contacts td {
        padding: 12px 0px 12px 18px;
        font-size: 12px;

    }

    .contacts img {
        height: 35px;
        width: 35px;
        border-radius: 50%;
    }

    .invoices table {
        border-collapse: collapse;
        width: 100%;
    }


    .invoices th{
        font-size: 12px;
        padding: 8px;
        padding-left: 18px;
        text-align: left;
        border-bottom: 2px solid #ddd;
    }


    .invoices td {
        padding: 12px 0px 12px 18px;
        font-size: 12px;
        border-bottom: 1px solid #ddd;
    }

    .modal {
        /*display: none; !* Hidden by default *!*/
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */

    }

    /* Modal Content */
    .modal-content {
        background-color: #fefefe;
        margin: auto;
        border: 1px solid #888;
        width: 65%;

    }

    /* The Close Button */
    .close {
        color: #aaaaaa;
        float: right;
        font-size: 28px;
        font-weight: bold;
    }

    .close:hover,
    .close:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
    }


    @media (max-width:991px) {
        #details1 {
            width: 100% !important;
        }

        #details2 {
            width: 100% !important;
        }
    }
    @media (max-width:767px) {
        .sidebar{
            padding-top: 0;
            height: 70px;
        }
        .navbar-top-links > li > a {
            padding-top: 28px !important;
        }
        .bootstrap-select:not([class*=span]):not([class*=col-]):not([class*=form-control]):not(.input-group-btn){
            margin-top: 18px;
        }
    }
</style>

@section('content')

    <div class="container" style="width: 100%">

        <div class="row">


            {{--        @include('admin.clients.client_header')--}}

            <div class="col-xs-3 details" id="details1">
                <h4>COMPANY DETAILS</h4>
                <hr style="background-color: #4DA9F3; height: 1.2px; margin-left: -8px; margin-right: -8px">
                <div>
                    <h5>COMPANY NAME:</h5>
                    <p>Business Plaza Nederland</p>
                    <hr>

                    <h5>PRIMARY CONTACT:</h5>
                    <p>Arther Menke</p>
                    <hr>

                    <h5>EMAIL:</h5>
                    <p>aurther@businessplazanederland.nl</p>
                    <hr>

                    <h5>WEBSITE:</h5>
                    <p><a href=""> www.businessplazanederland.nl </a></p>
                    <hr>

                    <h5>PHONE:</h5>
                    <p>+31 (0)85-2738131</p>
                    <hr>

                    <h5>MOBILE:</h5>
                    <p> - </p>
                    <h5>ADDRESS:</h5>
                    <p> Kompasstraat 3 </p>
                    <hr>

                    <h5>ZIP CODE:</h5>
                    <p>2901 AM</p>
                    <hr>

                    <h5>CITY:</h5>
                    <p>Capelle aan den |Jsse|</p>
                    <hr>

                    <h5>COMPANY NAME</h5>
                    <p>BUSINESS PLAZA NADERLAND</p>
                    <hr>

                    <h5>COUNTRY:</h5>
                    <p>Nederland</p>
                    <hr>

                    <h5>PROVINCE/STATE:</h5>
                    <p>-</p>
                    <hr>

                    <h5>COUSTOM ACCOUNTED:</h5>
                    <p>-</p>
                    <hr>

                    <h5>SOCIAL MEDIA</h5>
                    <p></p>

                </div>
            </div>

            <div class="col-xs-9" id="details2">

                <section>
                    <div class="sttabs tabs-style-line">

                        @include('admin.clients.tabs')

                        <hr style="background-color: #4DA9F3; height: 1.2px; width: 760px; margin-top: -2px;">

                        <div class="content-wrap">
                            <section id="section-line-1" class="show">
                                <div class="row">

                                    <div class="col-xs-12">
                                        <div class="white-box">
                                            <div class="row" style="border: 1px solid silver">
                                                <h6>NOTES</h6>
                                                <hr style="background-color: #4DA9F3; height: 1.2px;">
                                                <p style="height: 100px"></p>
                                            </div>

                                            <div class="row contacts" style="margin-top: 10px; border: 1px solid silver">
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <h6>CONTACTS</h6>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <button id="myBtn" style="float: right; height: 25px; border: none; margin: 10px 10px 0px 0px; border-radius: 4px; background-color: #4DA9F3; color: white; font-weight: bold;">ADD CONTACT</button>
                                                        <!-- The Modal -->
                                                        <div id="myModal" class="modal">
                                                            <!-- Modal content -->
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 style="color: white;">Add Contact <span class="close" data-dismiss="modal">&times;</span></h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <div class="container">
                                                                        <div class="col-md-12">
                                                                            <form id="addContact" method="POST">
                                                                                @csrf
                                                                                <input type="hidden" name="user_id" value="{{$client->id}}" />
                                                                                <div class="row">
                                                                                    <div class="form-body">
                                                                                            <div class="col-md-4 ">
                                                                                                <div class="form-group">
                                                                                                    <label>@lang('modules.contacts.contactName')</label>
                                                                                                    <input type="text" name="contact_name" id="contact_name" class="form-control">
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-4 ">
                                                                                                <div class="form-group">
                                                                                                    <label>@lang('app.phone')</label>
                                                                                                    <input id="phone" name="phone" type="tel" class="form-control">
                                                                                                </div>
                                                                                            </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="form-body">
                                                                                        <div class="col-md-4 ">
                                                                                            <div class="form-group">
                                                                                                <label>@lang('app.email')</label>
                                                                                                <input id="email" name="email" type="email" class="form-control" >
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-actions m-t-30">
                                                                                    <button type="button" id="save-form" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr style="background-color: #4DA9F3; height: 1.2px;">
                                                @if(Session::has('messages.clientDeleted'))
                                                    <div class="sweet-alert showSweetAlert visible" role="alert">
                                                        {{Session::get('messages.clientDeleted')}}
                                                    </div>
                                                @endif
                                                <div class="table-responsive">
                                                <table>
                                                    <tr>
                                                        <th><i class="glyphicon glyphicon-arrow-up"></i></th>
                                                        <th>Name</th>
                                                        <th><i class="glyphicon glyphicon-arrow-up"></i></th>
                                                        <th>Phone</th>
                                                        <th><i class="glyphicon glyphicon-arrow-up"></i></th>
                                                        <th>Email</th>
                                                        <th><i class="glyphicon glyphicon-arrow-up"></i></th>
                                                        <th>My Action</th>
                                                    </tr>

                                                    @foreach($contacts as $contact)
                                                        <tr>
                                                        <td><img src="{{asset('css/img/img_avatar.png')}}" alt=""></td>
                                                        <td>{{ ucwords($contact->contact_name) }}</td>
                                                        <td></td>
                                                            <td>{{ $contact->phone}}</td>
                                                        <td></td>
                                                        <td>{{ $contact->email }}</td>
                                                        <td>{{ $contact->action}}</td>
                                                           <td>
                                                               <a> <i data-toggle="modal" data-target="#myModal1-{{$contact->id}}" class="glyphicon glyphicon-edit"></i></a>
                                                               <a href="{{url('admin/clients/delete/contacts/'.$contact->id)}}"><i class="glyphicon glyphicon-remove"></i></a>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <div id="myModal1-{{$contact->id}}" class="modal">
                                                            <!-- Modal content -->
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 style="color: white;">Update Contact<span class="close" data-dismiss="modal">&times;</span></h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <div class="container">
                                                                        <div class="col-md-12">
                                                                            <form id="updateContact" method="post" action="{{url('/admin/clients/update/contacts/'.$contact->id)}}">
                                                                                @csrf
                                                                                <div class="row">
                                                                                    <div class="form-body">
                                                                                        <div class="col-md-4 ">
                                                                                            <div class="form-group">
                                                                                                <label>@lang('modules.contacts.contactName')</label>
                                                                                                <input type="text" value="{{$contact->contact_name}}" name="contact_name" id="contact_name" class="form-control">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-4 ">
                                                                                            <div class="form-group">
                                                                                                <label>@lang('app.phone')</label>
                                                                                                <input id="phone" value="{{$contact->phone}}" name="phone" type="tel" class="form-control">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="form-body">
                                                                                        <div class="col-md-4 ">
                                                                                            <div class="form-group">
                                                                                                <label>@lang('app.email')</label>
                                                                                                <input id="email" value="{{$contact->email}}" name="email" type="email" class="form-control" >
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-actions m-t-30">
                                                                                    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.update')</button>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    @endforeach


                                                </table>
                                                </div>
                                            </div>

                                            <div class="row project" style="margin-top: 10px; border: 1px solid silver">
                                                <h6>Projects</h6>
                                                <hr style="background-color: #4DA9F3; height: 1.2px;">

                                                <div class="table-responsive" >
                                                <table>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>PROJECT NAME</th>
                                                        <th>STARTED ON</th>
                                                        <th>DEAD LINE</th>
                                                        <th><i class="glyphicon glyphicon-arrow-up"></i></th>
                                                    </tr>
                                                    @forelse($client->projects as $key=>$project)
                                                    <tr>
                                                        <td>{{ $key+1 }}</td>
                                                        <td>{{ ucwords($project->project_name) }}</td>
                                                        <td>{{ $project->start_date->format($global->date_format) }}</td>
                                                        <td>@if($project->deadline){{ $project->deadline->format($global->date_format) }}@else - @endif</td>
                                                        <td><a href="{{ route('admin.projects.show', $project->id) }}" class="label label-info">@lang('modules.client.viewDetails')</a></td>
                                                    </tr>
                                                    @empty
                                                        <tr>
                                                            <td colspan="4">@lang('messages.noProjectFound')</td>
                                                        </tr>
                                                    @endforelse
                                                </table>
                                                </div>
                                            </div>

                                            <div class="row invoices" style="margin-top: 10px; border: 1px solid silver">
                                                <h6>INVOICES</h6>
                                                <hr style="background-color: #4DA9F3; height: 1.2px;">

                                                <div class="table-responsive">
                                                <table>
                                                    @forelse($invoice as $invoices)
                                                    <tr>
                                                        <td>Invoice # {{$invoices->invoice_number}}</td>
                                                        <td> </td>
                                                        <td> </td>
                                                        <td> </td>
                                                        <td> </td>
                                                        <td> </td>
                                                        <td> </td>
                                                        <td>$ {{$invoices->sub_total}}</td>
                                                        <td> </td>
                                                        <td><i class="glyphicon glyphicon-download-alt"></i>
                                        {{\Carbon\Carbon::parse($invoices->created_at)->format('m M, y')}}
                                                        </td>
                                                    </tr>
                                                    @empty
                                                        <tr>No Data Found ... !</tr>
                                                    @endforelse
                                                </table>
                                                </div>
                                            </div>
                                        </div>

                        </div>
                    </div>
                </section>
            </div><!-- /content -->
        </div><!-- /tabs -->
        </section>
    </div>


    </div>
    </div>
    <!-- .row -->

@endsection

@push('footer-script')
    <script>
        $('ul.showClientTabs .clientProfile').addClass('tab-current');
    </script>
    <script>
        // Get the modal
        var modal = document.getElementById("myModal");

        // Get the button that opens the modal
        var btn = document.getElementById("myBtn");

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];

        // When the user clicks the button, open the modal
        btn.onclick = function() {
            modal.style.display = "block";
        }

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }

        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{route('admin.contacts.store')}}',
                container: '#addContact',
                type: "POST",
                data: $('#addContact').serialize(),
                success: function (data) {
                    if(data.status == 'success'){
                        modal.style.display = "none";
                        // $('#addContact').toggleClass('hide', 'show');
                        // table._fnDraw();
                    }
                }
            })
        });
    </script>
    <script>
        // Get the modal
        var modal1 = document.getElementById("myModal1");

        // Get the button that opens the modal
        var btn1 = document.getElementById("myBtn1");

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];

        // When the user clicks the button, open the modal
        btn1.onclick = function() {
            modal.style.display = "block";
        }

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal1) {
                modal1.style.display = "none";
            }
        }

        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{route('admin.contacts.store')}}',
                container: '#addContact',
                type: "POST",
                data: $('#addContact').serialize(),
                success: function (data) {
                    if(data.status == 'success'){
                        modal.style.display = "none";
                        // $('#addContact').toggleClass('hide', 'show');
                        // table._fnDraw();
                    }
                }
            })
        });
    </script>
@endpush
